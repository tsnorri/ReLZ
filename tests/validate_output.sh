#!/usr/bin/env bash
set -o errexit
set -o nounset
set -o pipefail 

source "./utils.sh"
source "./tests_config.sh"

rm -f tmp.*
#N_CHUNKS=10
N_CHUNKS=1
# ch=30 will test feature to allow chunks shorter than ref.
N_THREADS=1



MAX_MEM_MB=1

utils_assert_file_exists ${ReLZ_BIN} 

COMPRESSED_FILE="tmp.relz.$RANDOM.PID${PROCID}"
DECOMPRESSED_FILE="tmp.decomp.$RANDOM.PID${PROCID}"
rm -f ${COMPRESSED_FILE}
rm -f ${DECOMPRESSED_FILE}

rm -f ${DATA_DIR}/*TMP*
rm -f ${DATA_DIR}/*rlzp
rm -f EINS* INF.*

${ReLZ_BIN} ${DATA_DIR}/einstein.en.sample.txt -v4 -d3 -l1000 -s -m1  -o EINS.rlzpp
utils_assert_equal_files EINS.rlzpp ./expected_results/EINS.rlzpp
rm -f EINS* INF.*

${ReLZ_BIN} ${DATA_DIR}/einstein.en.sample.txt -v4 -d1 -l1000 -s -m1  -o EINS_D1.rlzpp
utils_assert_equal_files EINS_D1.rlzpp ./expected_results/EINS_D1.rlzpp
rm -f EINS* INF.*

${ReLZ_BIN} ${DATA_DIR}/influenza.4M -v4 -d3 -l1000 -s -m1  -o INF.rlzpp
utils_assert_equal_files INF.rlzpp ./expected_results/INF.rlzpp
rm -f EINS* INF.*

${ReLZ_BIN} ${DATA_DIR}/einstein.en.sample.txt -v4 -d3 -l300  -o EINS.lz77
utils_assert_equal_files EINS.lz77 ./expected_results/EINS.lz77
rm -f EINS* INF.*

rm -f ${DATA_DIR}/*TMP*
rm -f ${DATA_DIR}/*rlzp
rm -f ${DATA_DIR}/*.backup
rm -f EINS* INF.

utils_success_exit
