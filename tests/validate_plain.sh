#!/usr/bin/env bash
set -o errexit
set -o nounset
set -o pipefail 

source "./utils.sh"
source "./tests_config.sh"

rm -f tmp.*
#N_CHUNKS=10
N_CHUNKS=20  
# ch=30 will test feature to allow chunks shorter than ref.
N_THREADS=4

INPUT_FILE=${DATA_DIR}/einstein.en.sample.txt
utils_assert_file_exists ${INPUT_FILE}


MAX_MEM_MB=100
REF_SIZE_MB=1

utils_assert_file_exists ${RLZ_FLEX} 
utils_assert_file_exists ${DECODER64_BIN} 

COMPRESSED_FILE="tmp.relz.$RANDOM.PID${PROCID}"
DECOMPRESSED_FILE="tmp.decomp.$RANDOM.PID${PROCID}"
rm -f ${COMPRESSED_FILE}
rm -f ${DECOMPRESSED_FILE}

# 1 4 8
# 8 fails because of bug in SA parallel range, only takes half the alphabet
for B in 1 ; do
	WIDTH_FLAG="-b ${B}"
	echo "Encoding..."
	echo "Line: ${RLZ_FLEX} ${INPUT_FILE} -l ${REF_SIZE_MB} -o ${COMPRESSED_FILE} -c ${N_CHUNKS} -t ${N_THREADS} -m ${MAX_MEM_MB} ${WIDTH_FLAG}"
	${RLZ_FLEX} ${INPUT_FILE} -l ${REF_SIZE_MB} -o ${COMPRESSED_FILE} -c ${N_CHUNKS} -t ${N_THREADS} -m ${MAX_MEM_MB} ${WIDTH_FLAG}
	echo "Decoding..."
	${DECODER64_BIN}  ${COMPRESSED_FILE} ${DECOMPRESSED_FILE} ${WIDTH_FLAG}
	echo "Verifying (diff)..."
	cmp ${DECOMPRESSED_FILE} ${INPUT_FILE}
	#rm -f tmp.*
	echo "---"
	echo "ok"
	echo "---"
done
utils_success_exit
