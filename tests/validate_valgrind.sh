#!/usr/bin/env bash
set -o errexit
set -o nounset
set -o pipefail 

source "./utils.sh"
source "./tests_config.sh"

rm -f tmp.*
#N_CHUNKS=10
N_CHUNKS=1
# ch=30 will test feature to allow chunks shorter than ref.
N_THREADS=1



MAX_MEM_MB=1

utils_assert_file_exists ${ReLZ_BIN} 

COMPRESSED_FILE="tmp.relz.$RANDOM.PID${PROCID}"
DECOMPRESSED_FILE="tmp.decomp.$RANDOM.PID${PROCID}"
rm -f ${COMPRESSED_FILE}
rm -f ${DECOMPRESSED_FILE}

rm -f ${DATA_DIR}/*TMP*
rm -f ${DATA_DIR}/*rlzp

valgrind --leak-check=full --vgdb-error=1 --show-leak-kinds=all ${ReLZ_BIN} ${DATA_DIR}/influenza.4M -v4 -d3 -l1000 -s -m1

rm -f ${DATA_DIR}/*TMP*
rm -f ${DATA_DIR}/*rlzp
rm -f ${DATA_DIR}/*.backup


utils_success_exit
