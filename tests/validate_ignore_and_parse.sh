#!/usr/bin/env bash
set -o errexit
set -o nounset
set -o pipefail 

source "./utils.sh"
source "./tests_config.sh"

rm -f tmp.*
N_CHUNKS=20  
N_THREADS=4


INPUT_FILE=${DATA_DIR}/einstein.mod.txt
utils_assert_file_exists ${INPUT_FILE}


MAX_MEM_MB=100
#REF_SIZE=24   triggers issue in divsufsort
#REF_SIZE=96 #also...
REF_SIZE=104

utils_assert_file_exists ${RLZ_FLEX} 
utils_assert_file_exists ${DECODER64_BIN} 


## If -s  is removed from code, M should be added to the tailc command
for IGNORE in 0 16 80; do
  for B in 1 8; do
    COMPRESSED_FILE="tmp.relz.$RANDOM.PID${PROCID}"
    DECOMPRESSED_FILE="tmp.decomp.$RANDOM.PID${PROCID}"
    rm -f ${COMPRESSED_FILE}
    rm -f ${DECOMPRESSED_FILE}
    echo "B=$B ; IGNORE=${IGNORE}"
    WIDTH_FLAG="-b ${B}"
    echo "Encoding..."
    echo "Line: ${RLZ_FLEX} ${INPUT_FILE} -l ${REF_SIZE} -o ${COMPRESSED_FILE} -c ${N_CHUNKS} -t ${N_THREADS} -m ${MAX_MEM_MB} -I ${IGNORE} ${WIDTH_FLAG}  -s"
    ${RLZ_FLEX} ${INPUT_FILE} -l ${REF_SIZE} -o ${COMPRESSED_FILE} -c ${N_CHUNKS} -t ${N_THREADS} -m ${MAX_MEM_MB} -I ${IGNORE} ${WIDTH_FLAG}  -s
    echo "Decoding..."
    ${DECODER64_BIN}  ${COMPRESSED_FILE} ${DECOMPRESSED_FILE} ${WIDTH_FLAG}

    #TRUNCATED_FILE=tmp.truncated
    #IGNORE1BASED=$((IGNORE+1))
    #tail -c +${IGNORE1BASED} ${INPUT_FILE} > ${TRUNCATED_FILE}
    echo "Verifying (diff)..."
    echo "Comparing those: ${DECOMPRESSED_FILE} ${INPUT_FILE}"
    cmp ${DECOMPRESSED_FILE} ${INPUT_FILE}

    echo "---"
    echo "ok"
    echo "---"
  done
done
utils_success_exit
