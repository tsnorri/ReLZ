/* ReLZ
    Copyright (C) 2018 Daniel Valenzuela

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see http://www.gnu.org/licenses/ .
*/
#include <cerrno>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <cstdint>
#include <string>
#include <sstream>
#include <fstream>
#include <algorithm>
#include <vector>
#include <iostream>
#include <sdsl/vectors.hpp>
#include <sdsl/coder_elias_delta.hpp>
#include <sdsl/coder_elias_gamma.hpp>
#include <unistd.h>
#include <sys/time.h>
#include "codecfactory.h"  // FastPFor
#include "./parse_writer.h"
#include "./parse_streamer.h"
#include "./parse_tools.h"
#include "./random_access_reader.h"
#include "./random_access_reader_disk.h"
#include "./random_access_reader_im.h"
#include "./tools.h"

using std::cout;
using std::cerr;
using std::endl;
using std::vector;
using std::ofstream;
using std::string;

vector<uint64_t> ParseTools::GetParseData64(string filename, PointerType pointer_type) {
  vector<uint64_t> answer;
  ParseStreamer * parse = new ParseStreamer(filename);
  uint64_t curr_pos = 0;
  while (!parse->isEmpty()) {
    Factor curr = parse->getNextFactor();
    uint64_t pos = curr.pos;
    uint64_t len = curr.len;
    uint64_t ptr;
    if (len && pointer_type == PointerType::DIST) {
      ptr = curr_pos - pos;
    } else {
      ptr = pos;
    }
    answer.push_back(ptr);
    answer.push_back(len);
    
    curr_pos += (len ? len : 1);
  }
  delete(parse);
  return answer;
}

void ParseTools::GetParse(string input_filename, 
                          PointerType pointer_type,
                          vector<uint64_t> &ptrs,
                          vector<uint32_t> &lens) {
  ParseStreamer input_parse(input_filename);
  uint64_t curr_pos = 0;
  while (!input_parse.isEmpty()) {
    Factor curr = input_parse.getNextFactor();
    uint64_t pos = curr.pos;
    uint64_t len = curr.len;
    uint64_t ptr;
    if (len && pointer_type == PointerType::DIST) {
      assert(curr_pos > pos);
      ptr = curr_pos - pos;
    } else {
      ptr = pos;
    }
    
    ptrs.push_back(ptr);
    lens.push_back(len);
    curr_pos += (len ? len : 1);
  }
}

FILE * ParseTools::open_file(const char *path, size_t *n) {
  FILE *fp = Tools::open_file_or_die(path, "r");
  assert(fp);
  fseek(fp, 0L, SEEK_END);
  *n = (size_t)ftell(fp);
  fseek(fp, 0L, SEEK_SET);
  return fp;
}

// 
void ParseTools::EncodeFinalOutput(string input_filename,
                                   string output_filename,
                                   EncodingType final_encode, 
                                   PointerType pointer_type) {
  //if (final_encode == EncodingType::VBYTE && pointer_type == PointerType::POS) {
  //  rename input->output
  //}
  srand(time(0));
  if (final_encode == EncodingType::PLAIN) {
    ParseTools::EncodePlain(input_filename, output_filename, pointer_type);  
  } else if (final_encode == EncodingType::ELIAS) {
    ParseTools::EncodeElias(input_filename, output_filename, pointer_type);  
  } else if (final_encode == EncodingType::VBYTE) {
    ParseTools::EncodeVbyte(input_filename, output_filename, pointer_type);  
  } else if (final_encode == EncodingType::MAX) {
    ParseTools::EncodeMax(input_filename, output_filename, pointer_type);  
  } else  {
    cerr << "Impossible happened, aborting" << endl;
    exit(33);
  }
  std::cerr << "output_filename: " << output_filename << '\n';
  Tools::remove_file_or_die(input_filename);
}

void ParseTools::RenameMultiOutput(string original_prefix, string new_prefix) {
  for (std::string suffix : MULTI_SUFFIXES) {
    cerr << "Suffix: " << suffix << endl;
    string curr_filename = original_prefix + suffix;
    string new_filename = new_prefix + suffix;
    Tools::rename_file_or_die(curr_filename, new_filename);
  }
}

void ParseTools::EncodePlain(string input_filename, string output_filename, PointerType pointer_type) {
  FILE * out_file = fopen(output_filename.c_str(), "w");
  if (!out_file) {
    cerr << "Problem opening " << output_filename << endl;
    exit(33);
  }
  ParseStreamer * parse = new ParseStreamer(input_filename);
  uint64_t curr_pos = 0;
  while (!parse->isEmpty()) {
    Factor curr = parse->getNextFactor();
    uint64_t pos = curr.pos;
    uint64_t len = curr.len;
    uint64_t ptr;
    if (len && pointer_type == PointerType::DIST) {
      assert(curr_pos > pos);
      ptr = curr_pos - pos;
    } else {
      ptr = pos;
    }
    if (1 != fwrite(&ptr, sizeof(ptr), 1, out_file)) {
      cerr << "Error writing to normalize output." << endl;
      exit(1);
    }
    if (1 != fwrite(&len, sizeof(len), 1, out_file)) {
      cerr << "Error writing to normalize output." << endl;
      exit(1);
    }
    uint64_t phrase_len = len ? len : 1;
    curr_pos += phrase_len;
  }
  fclose(out_file);
  delete(parse);
}

void ParseTools::EncodeElias(string input_filename, string output_filename, PointerType pointer_type) {
  // We will have the whole output in memory for a moment.
  vector<uint64_t> mydata = ParseTools::GetParseData64(input_filename, pointer_type);
  sdsl::int_vector<> tmp_int_vec(mydata.size());
  for (size_t i = 0; i < mydata.size(); i++) {
    tmp_int_vec[i] = mydata[i];
  }
  mydata.clear();
  //sdsl::vlc_vector<sdsl::coder::elias_delta<>, UINT32_MAX/4, 0> elias_vec(tmp_int_vec);
  sdsl::vlc_vector<sdsl::coder::elias_delta<>, 8192, 0> elias_vec(tmp_int_vec);
  store_to_file(elias_vec, output_filename.c_str());
}

void ParseTools::EncodeVbyte(string input_filename, string output_filename, PointerType pointer_type) {
  vector<uint64_t> mydata= ParseTools::GetParseData64(input_filename, pointer_type);
  
  ParseWriter curr_writer(output_filename);
  for (size_t i = 0; i < mydata.size(); i++) {
    curr_writer.EncodeAndWriteVbye(mydata[i]);
  }
}



void ParseTools::EncodeMax(string input_filename, string output_prefix, PointerType pointer_type) {
  vector<uint64_t> ptrs;
  vector<uint32_t> lens;

  ParseTools::GetParse(input_filename, pointer_type, ptrs, lens);
  assert(ptrs.size() == lens.size() && lens.size());
  /**/
  cerr << "***" << endl; 
  cerr << "***" << endl; 
  cerr << "***" << endl; 
  cerr << "POS MBE::" << endl;
  Tools::ReportMBESize(ptrs);
  cerr << "***" << endl; 
  cerr << "LEN MBE::" << endl;
  Tools::ReportMBESize(lens);
  cerr << "***" << endl; 
  cerr << "***" << endl; 
  cerr << "***" << endl; 
  /**/

  string pos_filename = output_prefix + ".pos";
  string len_filename = output_prefix + ".len";
  string z_filename = output_prefix + ".z";
  Tools::SaveNum(ptrs.size(), z_filename);
  
  //vector<uint32_t> new_ptrs(ptrs.size());
  //for (size_t i = 0; i < ptrs.size(); i++) new_ptrs[i] = ptrs[i];
  //ParseTools::SaveWithCodec(new_ptrs, pos_filename);
  
  SaveMixed(ptrs, pos_filename);
  
  ParseTools::SaveWithCodec(lens, len_filename);
 
  ParseTools::FSEOutput(output_prefix); 
}

void ParseTools::SaveWithVbyte(vector<uint32_t> mydata, string output_filename) {
  ParseWriter writer(output_filename);
  for (size_t i = 0; i < mydata.size(); i++) {
    writer.EncodeAndWriteVbye(mydata[i]);
  }
}

void ParseTools::SaveWithVbyte(uint32_t* data, size_t data_len, string output_filename) {
  ParseWriter writer(output_filename);
  for (size_t i = 0; i < data_len; i++) {
    writer.EncodeAndWriteVbye(data[i]);
  }
}

void ParseTools::SaveWithVbyte(uint64_t* data, size_t data_len, string output_filename) {
  ParseWriter writer(output_filename);
  for (size_t i = 0; i < data_len; i++) {
    writer.EncodeAndWriteVbye(data[i]);
  }
}

void ParseTools::SaveMixed(vector<uint64_t> mydata, string output_prefix) {
  uint32_t * data_thin = new uint32_t[mydata.size()];
  uint64_t * data_wide = new uint64_t[mydata.size()];
  
  size_t j = 0;
  for (size_t i = 0; i < mydata.size(); i++) {
    mydata[i] = mydata[i] + 1; 
    if (mydata[i] >= THRESH ) {
      data_wide[j] = mydata[i] - THRESH;
      data_thin[i] = 0;
      j++;
    } else {
      data_thin[i] = (uint32_t)mydata[i];
    }
  }
  size_t thin_len = mydata.size();
  size_t wide_len = j;
  string output_thin_filename = output_prefix + ".1";
  string output_wide_filename = output_prefix + ".2";
  
  ParseTools::SaveWithCodec(data_thin, thin_len, output_thin_filename);
  ParseTools::SaveWithVbyte(data_wide, wide_len, output_wide_filename);
  delete[] data_thin;
  delete[] data_wide;
}

void ParseTools::SaveWithCodec(vector<uint32_t> mydata, string output_filename) {
  SaveWithCodec(mydata.data(), mydata.size(), output_filename);
}

void ParseTools::SaveWithCodec(uint32_t *  data, size_t data_len, string output_filename) {
  // interesting codecs so far: simple16, simdfastpfor256 
  FastPForLib::IntegerCODEC &codec = *FastPForLib::CODECFactory::getFromName("simdfastpfor256");
  std::vector<uint32_t> compressed_data(data_len + 1024);

  size_t compressed_data_len = compressed_data.size();
  codec.encodeArray(data, data_len, compressed_data.data(),
                    compressed_data_len);
  compressed_data.resize(compressed_data_len);
  compressed_data.shrink_to_fit();
  std::cout << "You are using "
    << 32.0 * static_cast<double>(compressed_data.size()) /
    static_cast<double>(data_len)
    << " bits per integer. " << std::endl;

  FILE * fp = Tools::open_file_or_die(output_filename, "w");
  Tools::save_vector(fp, compressed_data);
  fclose(fp);
}

vector<uint64_t> ParseTools::DecodeMixed(string prefix, uint64_t nphrases) {
  string thin_filename = prefix + ".1";
  string wide_filename = prefix + ".2";
  
  vector<uint64_t> answer(nphrases);
  
  vector<uint32_t> thin = DecodeWithCodec(thin_filename, nphrases);
  io_private::async_vbyte_stream_reader<uint64_t> reader;
  reader.open(wide_filename);
  
  for (size_t i = 0; i < thin.size(); i++) {
    if (thin[i] == 0) {
      assert(!reader.empty());
      answer[i] = THRESH + reader.read();
    } else {
      answer[i] = thin[i];
    }
    answer[i] = answer[i] - 1;

  }
  assert(reader.empty());

  return answer;
}


uint32_t * ParseTools::DecodeWithCodec2(string filename, uint64_t nphrases) {
  FastPForLib::IntegerCODEC &codec = *FastPForLib::CODECFactory::getFromName("simdfastpfor256");
  
  uint32_t *compressed_data_tmp; 
  uint64_t data_len;
  Tools::read_typed<uint32_t>(filename.c_str(), compressed_data_tmp, data_len); 
  
  size_t recoveredsize = nphrases;
  uint32_t * tmp = new uint32_t[recoveredsize];
  
  codec.decodeArray(compressed_data_tmp, data_len,
                    tmp, recoveredsize);
  uint32_t * ans = new uint32_t[recoveredsize];
  for (size_t i = 0; i < recoveredsize; i++) ans[i] = tmp[i];
  delete[] tmp;
  
  return ans;
}

vector<uint32_t> ParseTools::DecodeWithCodec(string filename, uint64_t nphrases) {
  FastPForLib::IntegerCODEC &codec = *FastPForLib::CODECFactory::getFromName("simdfastpfor256");
  
  uint32_t *compressed_data_tmp; 
  uint64_t data_len;
  Tools::read_typed<uint32_t>(filename.c_str(), compressed_data_tmp, data_len); 
  std::vector<uint32_t> ans(nphrases);
  size_t recoveredsize = ans.size();
  
  codec.decodeArray(compressed_data_tmp, data_len,
                    ans.data(), recoveredsize);
  ans.resize(recoveredsize);
  
  return ans;
}

void ParseTools::FSEOutput(string prefix) {
  for (string suffix : {".len" , ".pos.1", ".pos.2"}) {
    string input_filename = prefix + suffix;
    string fse_command = string("yes | fse ");
    fse_command += input_filename;
    fse_command += " " + input_filename + ".fse";
#ifndef NDEBUG
    fse_command += " > " + input_filename + ".log 2>&1";
#else
    fse_command += " > /dev/null ";
#endif
    cerr << "ABOUT TO CALL:" << endl;
    cerr << fse_command << endl;
    cerr << "*****" << endl;
    fflush(stdout);
    fflush(stderr);
    int fse_return_value = system(fse_command.c_str());
    if (fse_return_value) {
      cerr << "Command failed, returned value: " << fse_return_value << endl;
      cerr << "Errno: " << std::strerror(errno) << endl;
      exit(EXIT_FAILURE);
    }
    Tools::remove_file_or_die(input_filename);
  }
}

// Called from split_to_vbyte which is used for decompression.
void ParseTools::DeFSEOutput(string prefix) {
  for (string suffix : {".len" , ".pos.1", ".pos.2"}) {
    string input_filename = prefix + suffix + ".fse";
    string output_filename = prefix + suffix;
    string fse_command = string("yes | fse -d ");
    fse_command += input_filename + " " + output_filename;
    fse_command += " > " + input_filename + ".log 2>&1";
    cerr << "ABOUT TO CALL:" << endl;
    cerr << fse_command << endl;
    cerr << "*****" << endl;
    fflush(stdout);
    fflush(stderr);
    int fse_return_value = system(fse_command.c_str());
    if (fse_return_value) {
      cerr << "Command failed, returned value: " << fse_return_value << endl;
      cerr << "Errno: " << std::strerror(errno) << endl;
      exit(EXIT_FAILURE);
    }
  }
}

void ParseTools::ComputeSumsStream(string parse_filename, string out_filename, size_t &estimated_ram_IM_MB) {
  ParseStreamer * rlz_parse = new ParseStreamer(parse_filename);
  ofstream out_fs(out_filename, std::ios::out | std::ios::binary);
  uint64_t sum = 0;
  out_fs.write(reinterpret_cast<char*>(&sum), sizeof(sum));
  int i = 1;
  size_t IM_bits = 0;
  while (!rlz_parse->isEmpty()) {
    i++;
    Factor curr = rlz_parse->getNextFactor();
    //assert(curr.len < text_len);
    uint64_t delta = (curr.len == 0) ? 1 : curr.len;
    IM_bits += Tools::bits(delta + 1);
    IM_bits += Tools::bits(Tools::bits(delta + 1));
    sum += delta;
    //assert(sum <= text_len);
    out_fs.write(reinterpret_cast<char*>(&sum), sizeof(sum));
  }
  estimated_ram_IM_MB = (IM_bits)/(8*1024*1024);
  out_fs.close();
  delete(rlz_parse);
}

size_t ParseTools::ReParseFile(string rlz_filename, string lz77_filename, string new_output_name, size_t max_memory_MB) {
  using pos_t = typename Factor::pos_t;
  //using len_t = typename Factor::len_t;
  size_t n_factors = 0;
  string sums_filename = rlz_filename + "tmp.sums";
  size_t estimated_ram_IM_MB;
  ComputeSumsStream(rlz_filename, sums_filename, estimated_ram_IM_MB);
  
  RandomAccessReader<uint64_t> * sums_reader;
  if (estimated_ram_IM_MB <= (size_t)max_memory_MB) {
    cerr << " Random Access to Sum Files IM (delta compressed)" << sums_filename << endl;
    sums_reader = new RandomAccessReaderIM<uint64_t>(sums_filename);
  } else {
    cerr << " Random Access to Sum Files from disk... will harm performance " << endl;
    sums_reader =  new RandomAccessReaderDisk<uint64_t>(sums_filename);
  }

  ParseWriter new_writer(new_output_name);
  ParseStreamer * rlz_parse = new ParseStreamer(rlz_filename);
  ParseStreamer * LZ_parse = new ParseStreamer(lz77_filename);

  uint64_t rlz_i = 0;
  while (!LZ_parse->isEmpty()) {
    Factor LZ_fact = LZ_parse->getNextFactor();
    uint64_t src_pos = LZ_fact.pos;
    uint64_t src_len = LZ_fact.len;
    if (src_len == 0 || src_len == 1) {
      n_factors++;
      Factor rlz_factor = rlz_parse->getNextFactor();
      new_writer.Save(&rlz_factor, 1);
      rlz_i++;
    } else {
      pos_t in_text_pos = sums_reader->get(src_pos);
      if (src_pos)
        assert(in_text_pos);
      pos_t in_text_len = 0;
      while (src_len != 0) {
        Factor rlz_factor = rlz_parse->getNextFactor();
        uint64_t phrase_len = (rlz_factor.len == 0) ? 1 : rlz_factor.len;
        in_text_len += phrase_len;
        src_pos += 1;
        src_len -= 1;
        rlz_i +=1;
      }
      //TODO(OPT): 
      //Potential optimization: replace previous loop with something like:
      //pos_t in_text_len = sums_reader->get(src_pos + src_len) - in_text_pos;
      //this also requires changing rlz_parse->getNextFactor() for a "map back" or getFactor(i)
      //this could be quicker or not: less opperations, but currently it reads the input linearly.
      //while the sums_reader may need to do a fseek.
      Factor new_factor;
      new_factor.pos = in_text_pos;
      new_factor.len = in_text_len;
      new_writer.Save(&new_factor, 1);
      n_factors++;
    }
  }
  new_writer.Close();
  delete(sums_reader);
  delete(rlz_parse);
  delete(LZ_parse);
  Tools::remove_file_or_die(rlz_filename);
  Tools::remove_file_or_die(lz77_filename);
  Tools::remove_file_or_die(sums_filename);
  return n_factors;
}

void ParseTools::VbyteToPackedIn64(std::string input_name, std::string packed_name) {
  using pos_t = typename Factor::pos_t;
  using len_t = typename Factor::len_t;
  len_t max_len = 0;
  pos_t max_pos_Z = 0;
  pos_t max_pos_NZ = 0;
  {
    ParseStreamer * input_parse = new ParseStreamer(input_name);
    while (!input_parse->isEmpty()) {
      Factor curr = input_parse->getNextFactor();
      max_len = std::max(max_len, curr.len);
      if (curr.len == 0)
        max_pos_Z = std::max(max_pos_Z, curr.pos);
      else
        max_pos_NZ = std::max(max_pos_NZ, curr.pos);
    }
    delete(input_parse);
  }

  uint32_t bits_per_len = Tools::bits(max_len);
  uint32_t bits_per_pos_Z = Tools::bits(max_pos_Z);
  uint32_t bits_per_pos_NZ = Tools::bits(max_pos_NZ);
  uint32_t total_bits = 1 + std::max(bits_per_pos_Z, bits_per_len + bits_per_pos_NZ);

  if (total_bits >=  64) {
    cerr << "Values to pack are too big, I cannot pack the pos,len pairs in 2^63 bits." << endl;
    exit(-1);
  }

  ParseStreamer * input_parse = new ParseStreamer(input_name);
  ofstream out_fs(packed_name, std::ios::out | std::ios::binary);
  while (!input_parse->isEmpty()) {
    Factor curr = input_parse->getNextFactor();
    uint64_t pos = curr.pos;
    uint64_t len = curr.len;
    uint64_t out_val;
    if (len) {
      out_val = (uint64_t)1 | (pos << 1) | (len << (bits_per_pos_NZ + 1));
      assert(out_val == (uint64_t)1 + (pos << 1) + (len << (bits_per_pos_NZ + 1)));
    } else {
      out_val = 0 | (pos << 1);
      assert(out_val == pos << 1);
    }
    assert(Tools::bits(out_val) <= total_bits);
    out_fs.write(reinterpret_cast<char*>(&out_val), sizeof(out_val));
  }
  delete(input_parse);
}
