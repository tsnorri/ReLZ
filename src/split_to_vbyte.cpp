/* ReLZ
    Copyright (C) 2018 Daniel Valenzuela

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see http://www.gnu.org/licenses/ .
*/
#include <stdint.h>
#include <getopt.h>
#include <omp.h>

#include <cstdlib>
#include <string>
#include <iostream>

#include "./iterative_parser.h"
#include "./parse_tools.h"


void print_help(char **argv);
void print_help(char **argv) {
	std::cerr << "usage: << " << argv[0] << " INPUT_PREFIX (.pos) (.len)  " << std::endl;
	std::cerr << std::endl;
	std::cerr << "Options: " << std::endl;

	std::cerr << " -o output name (default: OUTPUT.txt)" << std::endl;
	std::cerr << " -v verbosity level 0,1,.. (default 0)" << std::endl;
	std::cerr << " -p To indicate if pointers are stored as (pos,len) or (dist,len). Values: POS, DIST. Default: POS " << std::endl;
  /*
	cerr << " -d Maximum number of iterations. 0 => RLZ , 1=> RLZ + one iteration, 2,3,... (default: 5)" << endl;
	cerr << " -l Size of prefix used as reference in the first iteration, in MB (see -s)" << endl;
	cerr << " -m MAX_MEMORY_MB (default 8000). This is an approximate value, because e.g. the phrases are held in memory" << endl;
	cerr << " -s To indicate that the -l (also -I) parameter are in bytes" << endl;
	cerr << " -t N_THREADS (default: 1)" << endl;
	cerr << " -f indicated that input and reference are FASTA files" << endl;
	cerr << " -e Encodign scheme for the output. Values: PLAIN, VBYTE, ELAIAS" << endl;

	cerr << endl;
	cerr << " If -l is not given, MAX_MEMORY_MB will be use to estimate the largest acceptable value in the first iteration." << endl;
	cerr << endl;
	cerr << "Advanced options:" << endl;
	cerr << " -F forces 64 bits for sa_t, regardlless the length of the reference. For testing purposes" << endl;
	cerr << " -c N_CHUNKS  (default: N_THREADS)" << endl;
  */
}


void SplitToVbyte(std::string input_prefix, std::string output_filename, PointerType pointer_type);

int main(int argc, char **argv) {
  int verbosity = 0;
	std::string output_filename;
  PointerType pointer_type = PointerType::POS;
  int c;
	while ((c = getopt(argc, argv, "v:o:p:")) != -1) {
		switch(c) {
			case 'p':
        if (strcmp(optarg, "POS") == 0) {
          pointer_type = PointerType::POS;
        } else if (strcmp(optarg, "DIST") == 0) {
          pointer_type = PointerType::DIST;
        } else {
          std::cerr << "-p option not recognized." << std::endl;
          print_help(argv);
          exit(0);
        }
				break;
			case 'v':
				verbosity = atoi(optarg);
				break;
			case 'o':
				output_filename = std::string(optarg);
				break;
			default:
				print_help(argv);
				exit(0);
		}
	}
	int rest = argc - optind;
	if (rest != 1) {
		print_help(argv);
		exit(EXIT_FAILURE);
	}

	std::string input_prefix(argv[optind]);
	if (output_filename.size() == 0) {
		output_filename = "OUTPUT.txt";
	}
  
  if (verbosity) 
    std::cout << "output filename : " << output_filename << std::endl;

	SplitToVbyte(input_prefix, output_filename, pointer_type);
  fflush(stdout);
	fflush(stderr);
	return EXIT_SUCCESS;
}

void SplitToVbyte(std::string input_prefix, std::string output_filename, PointerType pointer_type) {
  std::string pos_filename = input_prefix + ".pos";
  std::string len_filename = input_prefix + ".len";
  std::string z_filename = input_prefix + ".z";
  
  ParseTools::DeFSEOutput(input_prefix); 

  uint64_t nphrases = Tools::LoadNum(z_filename);
 
  std::cerr << "Split to vbyte will process: " << nphrases << "phrases" << std::endl; 
  std::vector<uint32_t> data_len = ParseTools::DecodeWithCodec(len_filename, nphrases); 
  std::vector<uint64_t> data_pos = ParseTools::DecodeMixed(pos_filename, nphrases); 
  
  if (data_pos.size() != data_len.size()) {
    std::cerr << "Inconsistent pos and len files, aborting." << std::endl;
    exit(EXIT_FAILURE);
  }  
  ParseWriter output(output_filename);
  size_t curr_pos = 0;
  for (size_t i = 0; i < data_pos.size(); i++) {
    uint64_t ptr = data_pos[i];
    uint64_t len = data_len[i];
    uint64_t pos = ptr;
    if (len && pointer_type == PointerType::DIST) {
      pos = curr_pos - ptr;
    } 
    output.EncodeAndWriteVbye(pos);
    output.EncodeAndWriteVbye(len);
    curr_pos += (len ? len : 1);
  } 
}
