/* ReLZ
    Copyright (C) 2018 Daniel Valenzuela

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see http://www.gnu.org/licenses/ .
*/
#include "./tools.h"
#include <stdint.h>
#include <unistd.h>
#include <sys/time.h>
#include <errno.h>

#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <string>
#include <sstream>
#include <fstream>
#include <algorithm>
#include <vector>
#include <iostream>
#include "./parse_streamer.h"

using std::cout;
using std::cerr;
using std::endl;
using std::vector;
using std::ofstream;
using std::string;

bool Tools::is_breakline(uint8_t c) {
  if ((char)c == '\r' || (char)c == '\n')
    return true;
  else
    return false;
}

void Tools::fasta_to_seq(uint8_t *text, size_t text_len, size_t *seq_len) {
  size_t offset = 0;
  size_t i = 0;
  while (i + offset < text_len) {
    if (text[i+offset] == '>') {
      // cout << "One header skipped" << endl;
      while (i + offset < text_len &&
             !is_breakline(text[i+offset])) {
        offset++;
      }
      if (i+offset == text_len) {
        offset--;
        text[i+offset] = '\r';
      }
      continue;
    }
    if (is_breakline(text[i+offset])) {
      offset++;
      continue;
    }

    if (i + offset < text_len) {
      text[i] = text[i+offset];
    }
    i++;
  }
  assert(i + offset == text_len);

  *seq_len = text_len - offset;
  return;
}

// BEWARE: this depends on the LZ77 algorithm. currently based on KKP2.
size_t Tools::MaxRefFromMemBytes(size_t mem_bytes, size_t bytes_per_text_t, bool force_large) {
  size_t bytes_per_sa_t = force_large ? 8 : std::max((size_t)4, bytes_per_text_t);
  size_t factor = 2*bytes_per_sa_t+bytes_per_text_t;
  size_t n_symbols = mem_bytes/factor;

  if (force_large || n_symbols >= INT32_MAX) {
    // recompute with large sa_t
    assert(bytes_per_sa_t == 4);
    bytes_per_sa_t = 8;
    factor = 2*bytes_per_sa_t+bytes_per_text_t;
    n_symbols = mem_bytes/factor;
    if (n_symbols < INT32_MAX)
      n_symbols = INT32_MAX - 1;  // not enough memory to go for 64 bits, but enough for this.
  }
  // leave some room for blocks and phrases:
  return (n_symbols*9)/10;
}

size_t Tools::file_size_or_fail(const char *path) {
  std::ifstream in(path, std::ifstream::ate | std::ifstream::binary);
  if (!in) {
    cerr << "Problem opening file to get its size." << endl;
    cerr << "filename:" << path << endl;
    cerr << "Error: " << strerror(errno) << endl;
    exit(33);

  }
  return (size_t)in.tellg();
}

void Tools::rename_file_or_die(std::string first, std::string second) {
    if (std::rename(first.c_str(), second.c_str())) {
      cerr << "Problem renaming files in parse_tools, aborting." << endl;
      cerr << "First filename:" << first << endl;
      cerr << "Second filename:" << second << endl;
      cerr << "Error: " << strerror(errno) << endl;
      exit(33);
    }
}

void Tools::remove_file_or_die(string fname) {
  if (std::remove(fname.c_str())) {
    cerr << "Problem removing file  in tools, aborting." << endl;
    cerr << "Filename:" << fname << endl;
    cerr << "Error: " << strerror(errno) << endl;
    exit(33);
  }
}

FILE * Tools::open_file_or_die(string fname, string mode) {
  FILE *f = std::fopen(fname.c_str(), mode.c_str());
  if (!f) {
    std::perror(fname.c_str());
    std::exit(EXIT_FAILURE);
  }
  return f;
}

FILE * Tools::open_file_or_die(const char * path, string mode) {
  FILE *f = std::fopen(path, mode.c_str());
  if (!f) {
    std::perror(path);
    std::exit(EXIT_FAILURE);
  }
  return f;
}

void Tools::SaveNum(uint64_t num, std::string filename) {
  FILE * fp = Tools::open_file_or_die(filename.c_str(), "w");
  if (1 != fwrite(&num, sizeof(num), 1, fp)) {
    cerr << "Error writing  data to file" << endl;
    cerr << "Error: " << strerror(errno) << endl;
    exit(1);
  }
}

uint64_t Tools::LoadNum(std::string filename) {
  FILE * fp = Tools::open_file_or_die(filename.c_str(), "r");
  uint64_t num;
  if (1 != fread(&num, sizeof(num), 1, fp)) {
    cerr << "Error writing  data to file" << endl;
    cerr << "Error: " << strerror(errno) << endl;
    exit(1);
  }
  return num;
}

template<typename data_t>
void Tools::save_vector(FILE * fp, std::vector<data_t> vec) {
  size_t n = vec.size();
  if (n != fwrite(vec.data(), sizeof(data_t), n, fp)) {
    cerr << "Error writing  data to file" << endl;
    cerr << "Error: " << strerror(errno) << endl;
    exit(1);
  }
}

template<typename text_t>
void Tools::read_typed(const char *filename, text_t* &text, uint64_t &length) {
  std::fstream f(filename, std::fstream::in);
  if (f.fail()) {
    std::cerr << "\nError: cannot open file " << filename << "\n";
    std::exit(EXIT_FAILURE);
  }

  f.seekg(0, std::ios_base::end);
  int64_t byte_length = f.tellg();
  f.seekg(0, std::ios_base::beg);
  if ((size_t)byte_length % (sizeof(text_t))) {
    cerr << "Error:" << endl;
    cerr << "file length(in bytes)  = " << byte_length << endl;
    cerr << "file length(in bytes)  = " << sizeof(text_t) << endl;
    cerr << "Input file cannot be stored in a text_t sequence" << endl;
    exit(-1);
  }
  length = static_cast<uint64_t>(byte_length)/sizeof(text_t);

  text = new text_t[length];
  if (!text) {
    std::cerr << "\nError: allocation of " << length << " bytes failed\n";
    std::exit(EXIT_FAILURE);
  }

  f.read((char*)text, byte_length);
  if (!f) {
    std::cerr << "\nError: failed to read " << length << " bytes from file "
      << filename << ". Only " << f.gcount() << " could be read\n";
    std::exit(EXIT_FAILURE);
  }
  std::cerr << std::endl;
  f.close();
}

template<typename curr_t>
uint32_t Tools::bits(curr_t value) {
  uint32_t ans = 0;
  while (value) {
    value = (value >> 1);
    ans++;
  }
  return ans;
}

template<typename data_t>
void Tools::ReportMBESize(vector<data_t> data) {
  uint64_t minimum_len = 0;
  for (size_t i = 0; i < data.size(); i++) {
    minimum_len += Tools::bits(data[i]); 
  }
  std::cout << "MBE would use (bits):" << minimum_len << endl;
} 

template<typename text_t>
text_t *Tools::load_prefix(const char *path, size_t reference_length, size_t ignore_length) {
  FILE *fp = open_file_or_die(path, "r");
  assert(fp);

  fseek(fp, 0L, SEEK_END);

  size_t file_real_len = (size_t)ftell(fp);
  if (file_real_len < (reference_length + ignore_length)) {
    cerr << "file real len: " << file_real_len << endl;
    cerr << "reference len: " << reference_length << endl;
    cerr << "ignore len: " << ignore_length << endl;
    cerr << "Prefix size give for reference is larger than reference file" << endl;
    cerr << "Aborting." << endl;
    exit(33);
  }

  fseek(fp, (int64_t)(ignore_length*sizeof(text_t)), SEEK_SET);

  text_t *text = new text_t[reference_length + 1];

  if ((reference_length) != fread(text, sizeof(text_t), reference_length, fp)) {
    printf("ERR");
  }

  fclose(fp);

  return text;
}

FILE * Tools::open_file(const char *path, size_t *n) {
  FILE *fp = open_file_or_die(path, "r");
  assert(fp);
  fseek(fp, 0L, SEEK_END);
  *n = (size_t)ftell(fp);
  fseek(fp, 0L, SEEK_SET);
  return fp;
}

long double Tools::wclock() {
  timeval tim;
  gettimeofday(&tim, NULL);
  return tim.tv_sec + (tim.tv_usec / 1000000.0L);
}

template uint32_t Tools::bits<unsigned int>(unsigned int value);
template uint32_t Tools::bits<unsigned long>(unsigned long value);
template uint32_t Tools::bits<unsigned long long>(unsigned long long value);
template void Tools::ReportMBESize<uint32_t>(std::vector<uint32_t> data);
template void Tools::ReportMBESize<uint64_t>(std::vector<uint64_t> data);

template unsigned char * Tools::load_prefix<unsigned char>(const char *path, size_t reference_length, size_t ignore_length);
template uint32_t * Tools::load_prefix<uint32_t>(const char *path, size_t reference_length, size_t ignore_length);
template uint64_t * Tools::load_prefix<uint64_t>(const char *path, size_t reference_length, size_t ignore_length);
template void Tools::save_vector(FILE * fp, std::vector<uint64_t> vec);
template void Tools::save_vector(FILE * fp, std::vector<uint32_t> vec);
template void Tools::read_typed(const char *filename, uint64_t* &text, uint64_t &length);
template void Tools::read_typed(const char *filename, uint32_t* &text, uint64_t &length);
