/* ReLZ
   Copyright (C) 2018 Daniel Valenzuela

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see http://www.gnu.org/licenses/ .
   */
#include <stdint.h>
#include <getopt.h>
#include <omp.h>

#include <cstdlib>
#include <string>
#include <iostream>

#include "./common.h"
#include "./parse_tools.h"
#include "./parse_writer.h"
#include "./parse_streamer.h"
//#include "codecfactory.h"  // FastPFor

//#include "./coders/FastPFor/headers/codecfactory.h"

using std::cerr;
using std::string;
using std::endl;
using std::cout;
using std::vector;

void print_help(char **argv);
void print_help(char **argv) {
  cerr << "usage: << " << argv[0] << " INPUT_FILE " << endl;
  cerr << endl;
  cerr << "Options: " << endl;

  cerr << " -o prefix for output (default: INPUT_FILE.POS and INPUT_FILE.LEN" << endl;
  cerr << " -v verbosity level 0,1,.. (default 0)" << endl;
  /*
     cerr << " -d Maximum number of iterations. 0 => RLZ , 1=> RLZ + one iteration, 2,3,... (default: 5)" << endl;
     cerr << " -l Size of prefix used as reference in the first iteration, in MB (see -s)" << endl;
     cerr << " -m MAX_MEMORY_MB (default 8000). This is an approximate value, because e.g. the phrases are held in memory" << endl;
     cerr << " -s To indicate that the -l (also -I) parameter are in bytes" << endl;
     cerr << " -t N_THREADS (default: 1)" << endl;
     cerr << " -f indicated that input and reference are FASTA files" << endl;
     cerr << " -e Encodign scheme for the output. Values: PLAIN, VBYTE, ELAIAS" << endl;

     cerr << endl;
     cerr << " If -l is not given, MAX_MEMORY_MB will be use to estimate the largest acceptable value in the first iteration." << endl;
     cerr << endl;
     cerr << "Advanced options:" << endl;
     cerr << " -F forces 64 bits for sa_t, regardlless the length of the reference. For testing purposes" << endl;
     cerr << " -c N_CHUNKS  (default: N_THREADS)" << endl;
     */
}

int main(int argc, char **argv) {
  int verbosity = 0;
  string output_prefix;
  PointerType pointer_type = PointerType::POS;
  int c;
  while ((c = getopt(argc, argv, "v:o:p:")) != -1) {
    switch(c) {
			case 'p':
        if (strcmp(optarg, "POS") == 0) {
          pointer_type = PointerType::POS;
        } else if (strcmp(optarg, "DIST") == 0) {
          pointer_type = PointerType::DIST;
        } else {
          cerr << "-p option not recognized." << endl;
          print_help(argv);
          exit(0);
        }
				break;
      case 'v':
        verbosity = atoi(optarg);
        break;
      case 'o':
        output_prefix = string(optarg);
        break;
      default:
        print_help(argv);
        exit(0);
    }
  }
  int rest = argc - optind;
  if (rest != 1) {
    print_help(argv);
    exit(EXIT_FAILURE);
  }

  string input_string = string(argv[optind]);
  if (output_prefix.size() == 0) {
    output_prefix = input_string;
  }
  if (verbosity) 
    cout << "output prefix : " << output_prefix << endl;

  //VbyteToSplit(input_string, output_prefix);
  ParseTools::EncodeMax(input_string, output_prefix, pointer_type);
  fflush(stdout);
  fflush(stderr);
  return EXIT_SUCCESS;
}

/*
void EncodeVbyte(vector<uint32_t> mydata, string filename);
void EncodeVbyte(vector<uint32_t> mydata, string filename) {
  ParseWriter writer(filename);
  for (size_t i = 0; i < mydata.size(); i++) {
    writer.EncodeAndWriteVbye(mydata[i]);
  }
}


void EncodeWithCodec(vector<uint32_t> mydata, string filename);
void EncodeWithCodec(vector<uint32_t> mydata, string filename) {
  // interesting codecs so far: simple16, simdfastpfor256 
  FastPForLib::IntegerCODEC &codec = *FastPForLib::CODECFactory::getFromName("simdfastpfor256");
  std::vector<uint32_t> compressed_data(mydata.size() + 1024);

  size_t compressed_data_len = compressed_data.size();
  codec.encodeArray(mydata.data(), mydata.size(), compressed_data.data(),
                    compressed_data_len);
  compressed_data.resize(compressed_data_len);
  compressed_data.shrink_to_fit();
  std::cout << "You are using "
    << 32.0 * static_cast<double>(compressed_data.size()) /
    static_cast<double>(mydata.size())
    << " bits per integer. " << std::endl;

  FILE * fp = Tools::open_file_or_die(filename, "w");
  Tools::save_vector(fp, compressed_data);
  fclose(fp);
}
*/

void min_bin_size(vector<uint32_t> data);
void min_bin_size(vector<uint32_t> data) {
  uint64_t minimum_len = 0;
  for (size_t i = 0; i < data.size(); i++) {
    minimum_len += Tools::bits(data[i]); 
  }
  std::cout << "MBE would use "
    << static_cast<double>(minimum_len) /
    static_cast<double>(data.size())
    << " bits per integer. " << std::endl;
} 

vector<uint32_t> SimulateSS(vector<uint32_t> &pos_vec, vector<uint32_t> &len_vec);
vector<uint32_t> SimulateSS(vector<uint32_t> &pos_vec, vector<uint32_t> &len_vec) {
  if (pos_vec.size() != len_vec.size()) {
    cerr << "Different number of pos and lens, aborting" << endl;
    exit(EXIT_FAILURE);
  }
  vector<uint32_t> literals;
  vector<uint32_t> new_pos;
  new_pos.reserve(pos_vec.size());
  uint32_t threshold = 1;
  int count = 0;
  for (size_t i = 0; i < len_vec.size(); i++) {
    uint32_t curr_len = len_vec[i];
    if (curr_len <= threshold) {
      count++;
      for (size_t j = 0; j < curr_len; j++) {
        literals.push_back(11);
      }
    } else {
      new_pos.push_back(pos_vec[i]);
    }
  }
 
  pos_vec = new_pos;
  cerr << "Number of 'pos' values removed: " << count << endl;

  assert(pos_vec.size() + count == len_vec.size());
  assert(literals.size() >= count);

  cerr << literals.size() << " literals created." << endl;
  return literals;
}


