/* ReLZ
    Copyright (C) 2018 Daniel Valenzuela

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see http://www.gnu.org/licenses/ .
*/
#include <fstream>
#include <string>
#include <cassert>
#include "tools.h"
#include "random_access_reader_disk.h"
using std::string;


template <typename T>
RandomAccessReaderDisk<T>::RandomAccessReaderDisk(string filename) {
  in_fs.open(filename, std::ifstream::ate | std::ifstream::binary);
  __size = in_fs.tellg()/(int64_t)sizeof(Factor);
  in_fs.seekg (0, in_fs.beg);
  _i	= 0;	
}

template <typename T>
RandomAccessReaderDisk<T>::~RandomAccessReaderDisk() {
  in_fs.close();
  /*
     std::vector<std::pair<size_t, size_t>> pairs;
     for (auto itr = freqs.begin(); itr != freqs.end(); ++itr) {
     size_t v1 = itr->first;
     size_t v2 = itr->second;
     pairs.push_back(std::make_pair(v2,v1));
     }
     std::sort(pairs.begin(), pairs.end());
     std::reverse(pairs.begin(), pairs.end());
     std::cerr << "By Freqs" << std::endl;
     int count = 0;
     for (auto itr = pairs.begin(); itr != pairs.end() && count < 20; ++itr, count++) {
     std::cerr << itr->first << "," << itr->second << std::endl;
     }
     */
}

template class RandomAccessReader<uint64_t>;
template class RandomAccessReaderDisk<uint64_t>;
