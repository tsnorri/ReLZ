/* ReLZ
    Copyright (C) 2018 Daniel Valenzuela

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see http://www.gnu.org/licenses/ .
*/
#include <fstream>
#include <string>
#include <cassert>
#include "tools.h"
#include "random_access_reader_im.h"
using std::string;


template <typename T>
RandomAccessReaderIM<T>::RandomAccessReaderIM(string sums_filename) {
  sdsl::int_vector_buffer<> buffer(sums_filename, 
                                   std::ios_base::in, 
                                   1024*1024,  // buffer length.
                                   64, // 64 because ComputeSums saved them as 64 bits
                                   true);  
  encoded_data = sdsl::enc_vector<sdsl::coder::elias_gamma<>, 128, 0>(buffer);
  // std::cerr << "IM approach actually uses (MB)        :" << sdsl::size_in_mega_bytes(encoded_data) << std::endl;
  // std::cerr << "IM approach number of bits per element:" << 8*1024*1024*sdsl::size_in_mega_bytes(encoded_data)/encoded_data.size() << std::endl;
}

template <typename T>
T RandomAccessReaderIM<T>::get(size_t pos) {
  return encoded_data[pos];
}

template <typename T>
RandomAccessReaderIM<T>::~RandomAccessReaderIM() {
}

template class RandomAccessReader<uint64_t>;
template class RandomAccessReaderIM<uint64_t>;
