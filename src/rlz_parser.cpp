/* ReLZ
   Copyright (C) 2018 Daniel Valenzuela

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see http://www.gnu.org/licenses/ .
   */

/* *

   A flexible RLZ parser that can operate not only in chars, but also in integers.
   Specified through the text_t parameter.

 * */
#include "./rlz_parser.h"
#include <cstdint>
#include <cstring>
#include <cassert>
#include <cstdlib>
#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <string>
#include <utility>
#include <algorithm>

#include "./parse_tools.h"
#include "./compute_lz77.hpp"
#include "./fastametadata.hpp"
#include "./random_access_reader_disk.h"
#include "./random_access_reader_im.h"

using std::ofstream;
using std::pair;
using std::cin;
using std::cout;
using std::endl;
using std::vector;
using std::pair;
using std::string;
using FMD::FastaMetaData;

template <typename text_t, typename sa_t>
RlzParser<text_t, sa_t>::RlzParser() {
}

template <typename text_t, typename sa_t>
RlzParser<text_t, sa_t>::RlzParser(RlzParameters parameters) {
  this->is_already_parsed = false;
  parameters.Validate();
  this->input_filename = parameters.input_filename;
  this->output_filename = parameters.output_filename;
  this->reference_len = parameters.reference_len;
  this->ignore_len = parameters.ignore_len;
  this->n_threads = parameters.n_threads;
  this->n_partitions = parameters.n_partitions;
  this->max_memory_MB = parameters.max_memory_MB;
  this->is_fasta = parameters.is_fasta;
  if (is_fasta) {
    FastaMetaData *tmp;
    tmp = new FastaMetaData(string(input_filename));
    tmp->Save();
#ifndef NDEBUG
    tmp->Test();
#endif
    delete(tmp);
  }
  this->verbosity = parameters.verbosity;
  assert(reference_len > 0);

  if (Tools::bits(reference_len) >= 8*sizeof(sa_t)) {
    std::cerr << "WARNING, reference requieres more than " << 8*sizeof(sa_t) << " bits." << std::endl;
    std::cerr << "WARNING, reference requieres more than " << 8*sizeof(sa_t) << " bits." << std::endl;
    std::cerr << "Which is the longer we can handle." << std::endl;
    std::cerr << "We will truncate the reference length to fit in the limit." << std::endl;
    std::cerr << "If you want to try longer reference, compile with -DLARGEREFERENCES option." << std::endl;
    std::cerr << "If you already compiled with -DLARGEREFERENCES and get this message, " << std::endl;
    std::cerr << "sorry, we cannot handle more than 64 bits references." << std::endl;
    reference_len = (size_t)1 << (size_t)(8*sizeof(sa_t) - 1);
  }

  if (verbosity >= 2) {
    std::cout << "RLZ parser intitialized with:" << std::endl;
    std::cout << "Ignore     : " <<ignore_len << std::endl;
    std::cout << "Ref        : " << reference_len << std::endl;
    std::cout << "Partitions : " <<n_partitions << std::endl;
    std::cout << "Memory(MB) : " << max_memory_MB << std::endl;
    fflush(stdout);
  }
  skipped_area_factors = 0;
  lz77_factors = 0;
  rlz_factors = 0;
  total_factors = 0;
  text_len_bytes = Tools::file_size_or_fail(input_filename.c_str());
  assert((text_len_bytes % sizeof(text_t)) == 0);
  text_len = text_len_bytes/sizeof(text_t);

  this->writer.Open(output_filename);
}

template <typename text_t, typename sa_t>
void RlzParser<text_t, sa_t>::Parse() {
  if (ignore_len != 0)
    this->skipped_area_factors = NaiveParsePrefix();
  else
    this->skipped_area_factors = 0;

  if (verbosity >= 2) {
    cout << "Building SA..." << endl;
    fflush(stdout);
  }
  long double t1, t2;
  t1 = Tools::wclock();
  size_t block_len = ComputeBlockLen();

  block_reader.Init(input_filename, ignore_len*sizeof(text_t), block_len, verbosity);

  {
    text_t * reference_text = new text_t[reference_len];
    block_reader.LoadFirstBlock(reference_text, reference_len);
    size_t reference_len_real;
    if (is_fasta)
      Tools::fasta_to_seq((uint8_t*)reference_text, reference_len, &reference_len_real);
    else
      reference_len_real = reference_len;
    d.Init(reference_text, reference_len_real, ignore_len, n_threads);
  }
  if (verbosity >= 2) {
    cout << "Computing SA(ref)..." << endl;
    fflush(stdout);
  }
  d.BuildSA();
  t2 = Tools::wclock();
  time_sa = t2 - t1;

  if (verbosity >= 2) {
    cout << "Computing LZ77(ref)..." << endl;
    fflush(stdout);
  }
  t1 = Tools::wclock();
  this->lz77_factors = this->parse_ref();
  t2 = Tools::wclock();
  time_parse_ref = t2 - t1;

  if (verbosity >= 2) {
    cout << "Starting RLZ..." << endl;
    fflush(stdout);
  }
  t1 = Tools::wclock();
  block_reader.AsynchPreLoadNext();

  size_t block_offset = reference_len + ignore_len;
  size_t block_id = 0;

  while (block_offset < text_len) {
    assert(!block_reader.IsReady());
    size_t buffer_len = (block_offset + block_len < text_len) ? block_len : text_len - block_offset;
    rlz_factors += process_next_block(block_offset);

    block_offset += buffer_len;
    block_id++;
  }
  assert(block_reader.IsReady());
  total_factors = skipped_area_factors + lz77_factors + rlz_factors;
  t2 = Tools::wclock();
  time_rlz = t2 - t1;

  time_total = time_sa + time_parse_ref + time_rlz;
  this->block_reader.Close();
  this->writer.Close();
  this->d.Close();
  this->is_already_parsed = true;
}

template <typename text_t, typename sa_t>
size_t RlzParser<text_t, sa_t>::ComputeBlockLen() {
  assert(((size_t)max_memory_MB << (size_t)20) > d.size_in_bytes(reference_len));
  size_t block_len_bytes = ((size_t)max_memory_MB << (size_t)20) - d.size_in_bytes(reference_len);
  // this would lead to a "TIGHT" memory ussage.  But the phrases will be kept in memory for a while
  // so we need to estimate for that:
#ifndef LARGEREFERENCES
  block_len_bytes = (block_len_bytes*35)/100;
  block_len_bytes = std::max(block_len_bytes, (size_t)4 << (size_t)20);  // no less than 4 MB though.  // NOLINT
  block_len_bytes = std::min(block_len_bytes, (size_t)2 << (size_t)30);   // no more than 2 GB neither.  // NOLINT  //TODO: measure what is a better tight limit here.
#endif
  if (block_len_bytes > text_len_bytes) {
    block_len_bytes = text_len_bytes;
  }

  size_t block_len = block_len_bytes/sizeof(text_t);
  if (verbosity >= 2) {
    std::cout << "We will read blocks of size: " << block_len << std::endl;
    fflush(stdout);
  }
  return block_len;
}

template <typename text_t, typename sa_t>
size_t RlzParser<text_t, sa_t>::NaiveParsePrefix() {
  text_t * prefix = Tools::load_prefix<text_t>(input_filename.c_str(), ignore_len, 0);
  vector<Factor> parse(ignore_len);
  for (size_t i = 0; i < ignore_len; i++) {
    parse[i].pos = prefix[i];
    parse[i].len = 0;
  }
  this->writer.Save(&parse[0], ignore_len);
  delete[] prefix;
  return ignore_len;
}

template <typename text_t, typename sa_t>
size_t RlzParser<text_t, sa_t>::process_next_block(size_t block_offset) {
  size_t buffer_len;
  text_t * buffer = block_reader.GetNextBlock(&buffer_len);

  vector<vector<Factor>> factor_lists(n_partitions);
  if (verbosity >= 2) {
    std::cout << endl << "*" << std::endl;
    std::cout << "Starting parallel parsing in block...";
    fflush(stdout);
  }
  long double start_time = Tools::wclock();
#pragma omp parallel for
  for (size_t t = 0; t < n_partitions; t++) {
    // MAYBE encapuslate all this block...
    // NOTE: each thread indeed needs its own instance of FastaMetaData.
    // (not thread-safe)
    FastaMetaData *metadata;
    if (is_fasta) {
      metadata = new FastaMetaData();
      metadata->Load(input_filename +".metadata");
    } else {
      metadata = NULL;
    }
#ifndef NDEBUG
    if (metadata)
      metadata->Test();
#endif
    size_t starting_pos = t*(buffer_len/n_partitions);
    size_t length = (t != n_partitions - 1) ? (buffer_len/n_partitions) : buffer_len - starting_pos;
    size_t abs_starting_pos = starting_pos + block_offset;

    assert(d.n <= block_offset + starting_pos);

    chunk_parse(buffer+starting_pos, length, factor_lists[t], metadata, abs_starting_pos);
    if (metadata != NULL) {
      delete(metadata);
    }
  }
  long double end_time = Tools::wclock();
  if (verbosity >= 2) {
    std::cout << "Parallel parse in " << end_time - start_time << " (s)" << std::endl;
    fflush(stdout);
  }

  if (verbosity >= 2) {
    std::cout << "Writing phrases... " << std::endl;
    fflush(stdout);
  }
  start_time = Tools::wclock();
  size_t n_factors = flush_phrases(factor_lists);
  end_time = Tools::wclock();
  if (verbosity >= 2) {
    std::cout << "Phrases written  in " << end_time - start_time << " (s)" << std::endl;
    fflush(stdout);
  }
  return n_factors;
}

template <typename text_t, typename sa_t>
void RlzParser<text_t, sa_t>::chunk_parse(text_t* buffer,
                                          size_t length,
                                          std::vector<Factor> & ans,
                                          FastaMetaData * metadata,
                                          size_t abs_starting_pos) {
  if (metadata != NULL) {
    assert(sizeof(text_t) == sizeof(uint8_t));
    metadata->PartialFastaToSeq((uint8_t*)buffer, &length, abs_starting_pos);
  }

  size_t i = 0;
  while (i < length) {
    Factor factor = d.at(buffer + i, length - i);
    ans.push_back(factor);
    i += (factor.len > 0) ? factor.len : 1;
  }
  return;
}

template <typename text_t, typename sa_t>
void RlzParser<text_t, sa_t>::read_block(text_t *buffer,
                                         size_t buffer_len,
                                         size_t block_id,
                                         FILE * input_fp) {
  if (verbosity >= 2) {
    cout << "Reading block " << block_id << "...";
    fflush(stdout);
  }
  long double start_time = Tools::wclock();
  if (buffer_len != fread(buffer, sizeof(text_t), buffer_len, input_fp)) {
    printf("ERR");
    exit(-1);
  }
  long double end_time = Tools::wclock();
  if (verbosity >= 2) {
    cout << "Block read in " << end_time - start_time << " (s)" << endl;
    fflush(stdout);
  }
}

template <typename text_t, typename sa_t>
size_t RlzParser<text_t, sa_t>::flush_phrases(vector<vector<Factor>> factor_lists) {
  size_t n_factors = 0;
  for (size_t t = 0; t < factor_lists.size(); t++) {
    n_factors += factor_lists[t].size();

    size_t data_len = factor_lists[t].size();
    this->writer.Save(&(factor_lists[t][0]), data_len);
  }
  return n_factors;
}

template <typename text_t, typename sa_t>
RlzParser<text_t, sa_t>::~RlzParser() {
}

template <typename text_t, typename sa_t>
size_t RlzParser<text_t, sa_t>::parse_ref() {
  size_t n_factors;
  size_t offset = ignore_len;
  n_factors = kkp_parse_ref(d.d, d.sa, d.n, offset);
  return n_factors;
}

template <typename text_t, typename sa_t>
size_t RlzParser<text_t, sa_t>::kkp_parse_ref(text_t * seq,
                                              sa_t* sa,
                                              size_t seq_len,
                                              size_t offset) {
  using s_sa_t = typename std::make_signed<sa_t>::type;
  fflush(stdout);
  vector<pair<s_sa_t, s_sa_t>> ref_parsing;
  size_t n_phrases = compute_lz77::kkp2n<text_t, s_sa_t>(seq,
                                                         (s_sa_t*)sa,
                                                         (uint64_t)seq_len,
                                                         writer,
                                                         offset);
  return (size_t)n_phrases;
}

template <typename text_t, typename sa_t>
ParseStreamer * RlzParser<text_t, sa_t>::getParseStreamer() {
  assert(is_already_parsed);
  ParseStreamer * ans = new ParseStreamer(this->output_filename);
  return ans;
}

template <typename text_t, typename sa_t>
void RlzParser<text_t, sa_t>::ComputeSumsStream(string out_filename) {
  assert(is_already_parsed);
  ParseStreamer * rlz_parse = this->getParseStreamer();
  ofstream out_fs(out_filename, std::ios::out | std::ios::binary);
  uint64_t sum = 0;
  out_fs.write(reinterpret_cast<char*>(&sum), sizeof(sum));
  int i = 1;
  while (!rlz_parse->isEmpty()) {
    i++;
    Factor curr = rlz_parse->getNextFactor();
    assert(curr.len < text_len);
    uint64_t delta = (curr.len == 0) ? 1 : curr.len;
    sum += delta;
    assert(sum <= text_len);
    out_fs.write(reinterpret_cast<char*>(&sum), sizeof(sum));
  }
  out_fs.close();
  delete(rlz_parse);
}

template struct RlzParser<uint8_t, uint32_t>;
template struct RlzParser<uint8_t, uint64_t>;
template struct RlzParser<uint64_t, uint64_t>;
