/* ReLZ
   Copyright (C) 2018 Daniel Valenzuela

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see http://www.gnu.org/licenses/ .
*/
#ifndef ReLZ_SRC_DICTIONARY_H_
#define ReLZ_SRC_DICTIONARY_H_

#include <cassert>
#include <cstdlib>
#include <cstdint>
#include "./tools.h"

template <typename text_t, typename sa_t>
struct Dictionary {
  using pos_t = typename Factor::pos_t;
  using len_t = typename Factor::len_t;
  Dictionary();
  void Init(text_t * text, size_t text_len, size_t ignored_len, int n_threads);
  void BuildSA();
  void Close();
  ~Dictionary() {
  }

  inline Factor at(text_t *t, size_t tn) const {
    assert(t);
    assert(sa);
    size_t lhs = 0, rhs = n - 1;
    len_t match_len = 0;
    while (1) {
      if (match_len >= tn || match_len == UINT32_MAX) break;
      text_t next_char = t[match_len];
      size_t sp = lhs, ep = rhs, m = (sp + ep) / 2;
      while (sp < ep) {
        if (sa[m] + match_len < n && d[sa[m] + match_len] >= next_char) {
          ep = m;
        } else {
          sp = m + 1;
        }
        m = (sp + ep) / 2;
      }
      if (sa[sp] + match_len >= n || d[sa[sp] + match_len] != next_char) break;
      lhs = sp;
      ep = rhs;
      m = (sp + ep + 1) / 2;
      while (sp < ep) {
        if (sa[m] + match_len < n && d[sa[m] + match_len] <= next_char) {
          sp = m;
        } else {
          ep = m - 1;
        }
        m = (sp + ep + 1) / 2;
      }
      rhs = ep;
      ++match_len;
    }
    Factor factor = {(pos_t)sa[lhs], (len_t)match_len};
    if (match_len == 0) {
      factor.pos = t[0];
    } else {
      factor.pos += offset_ignored;
    }
    return factor;
  }

  size_t size_in_bytes();
  size_t size_in_bytes(size_t len);

  text_t *d;
  sa_t *sa;
  size_t n;
  size_t offset_ignored;
  bool is_fasta;
  int n_threads;
};

#endif  // ReLZ_SRC_DICTIONARY_H_
