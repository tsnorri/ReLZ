/* ReLZ
   Copyright (C) 2018 Daniel Valenzuela

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see http://www.gnu.org/licenses/ .
*/
#ifndef ReLZ_SRC_ITERATIVE_PARSER_H_
#define ReLZ_SRC_ITERATIVE_PARSER_H_

#include <cstdio>
#include <cstdlib>
#include <vector>
#include <string>

#include <algorithm>
#include <cstring>
#include <fstream>
#include <iostream>
#include <sstream>

#include "./tools.h"
#include "./rlz_parser.h"


class IterativeParser {
  public:
    IterativeParser();
    IterativeParser(std::string _input_filename,
                    std::string _output_filename,
                    EncodingType _final_encoding,
                    PointerType _pointer_type,
                    size_t _max_depth,
                    size_t _first_reference_len,
                    size_t _n_threads,
                    size_t _n_partitions,
                    size_t _max_memory_MB,
                    int _verbosity,
                    bool _is_fasta);
    void Parse();
    void printStats();

    ~IterativeParser();
  private:
    void RecursiveParsePhrases(std::string phrases_input,
                               std::string phrases_output,
                               size_t ready_factors,
                               size_t depth);
    void computeStats();
    bool is_fasta;
    std::string input_filename;
    std::string output_filename;
    size_t max_depth;
    size_t first_reference_len;
    size_t n_threads;
    size_t n_partitions;
    int verbosity;
    size_t max_memory_MB;
    EncodingType final_encoding;
    PointerType pointer_type;

    std::vector<std::string> level_reports;
    long double total_time;
    size_t n_factors;
    size_t bytes_input;
    size_t bytes_output;
    long double compression_ratio;
    long double bits_per_char;
    long double secs_per_MiB;
    long double secs_per_GiB;
    long double MiB_per_sec;
    long double GiB_per_sec;
  public:
    size_t getNFactors() const {
      return n_factors;
    }
    long double getCompressionRatio() {
      return compression_ratio;
    }
};
#endif  // ReLZ_SRC_ITERATIVE_PARSER_H_
