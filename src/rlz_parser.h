/* ReLZ
   Copyright (C) 2018 Daniel Valenzuela

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see http://www.gnu.org/licenses/ .
   */
#ifndef ReLZ_SRC_RLZ_PARSER_H_
#define ReLZ_SRC_RLZ_PARSER_H_

#include <cstdlib>
#include <string>
#include <vector>
#include <algorithm>
#include <cstring>
#include <fstream>
#include <iostream>

#include "./dictionary.h"
#include "./fastametadata.hpp"
#include "./parse_streamer.h"
#include "./parse_writer.h"
#include "./block_reader.h"
#include "./common.h"


struct RlzParameters {
  RlzParameters()
  {}
  RlzParameters(std::string _input_filename,
                std::string _output_filename,
                size_t _reference_len,
                size_t _ignore_len,
                size_t _n_threads,
                size_t _n_partitions,
                size_t _max_memory_MB,
                int _bytes_per_symbol,
                bool _force_large,
                int _verbosity,
                bool _is_fasta) {
    input_filename = _input_filename;
    output_filename = _output_filename;
    reference_len = _reference_len;
    ignore_len = _ignore_len;
    n_threads = _n_threads;
    n_partitions = _n_partitions;
    max_memory_MB = _max_memory_MB;
    bytes_per_symbol = _bytes_per_symbol;
    verbosity = _verbosity;
    force_large = _force_large;
    is_fasta = _is_fasta;
  }
  void Validate() {
    size_t input_len_bytes = Tools::file_size_or_fail(input_filename.c_str());
    assert((input_len_bytes % (size_t)bytes_per_symbol) == 0);
    assert(max_memory_MB);
    size_t max_ref = (size_t)Tools::MaxRefFromMemBytes(max_memory_MB*(size_t)(1024*1024),
                                                       (size_t)bytes_per_symbol,
                                                       force_large);
    if (reference_len == 0) {
      reference_len = max_ref;
      if (verbosity >= 3) {
        std::cerr << std::endl;
        std::cerr << "--- computing reference lenght based on memory limit:" << std::endl;
        std::cerr << "--- With " << max_memory_MB
          << "MB of  memory, the reference length will be: "
          << reference_len << " symbols" <<  std::endl;
      }
    } else {
      if (reference_len > max_ref) {
        std::cerr << "Specified reference length may require more than the specified RAM. " << std::endl;
        std::cerr << "Proceeding anyway, at your own risk. " << std::endl;
      }
    }

    size_t input_len = (size_t)input_len_bytes/(size_t)bytes_per_symbol;
    if (ignore_len > input_len) {
      std::cerr << "Ignore len is larger that the whole file." << std::endl;
      std::cerr << "Aborting" << std::endl;
      exit(33);
    }
    if ((input_len - ignore_len) < reference_len) {
      if (verbosity >= 3) {
        std::cerr << std::endl;
        std::cerr << "--- reference_len is longer than the text are to be parsed: adjusting it.";
      }
      reference_len = input_len - ignore_len;
    }
  }
  std::string input_filename;
  std::string output_filename;
  std::vector<std::string> output_suffixes;
  size_t reference_len;
  size_t ignore_len;
  size_t n_threads;
  size_t n_partitions;
  size_t max_memory_MB;
  int bytes_per_symbol;
  int verbosity;
  bool force_large;
  bool is_fasta;
};

template <typename text_t, typename sa_t>
struct RlzParser {
  using pos_t = typename Factor::pos_t;
  using len_t = typename Factor::len_t;
  RlzParser();

  explicit RlzParser(RlzParameters parameters);

  void Parse();

  void ReportFactors(std::string& output) {
    output = output + "Factors skipped      : " + std::to_string(skipped_area_factors) + "\n";
    output = output + "Factors 77'ed in ref : " + std::to_string(lz77_factors) + "\n";
    output = output + "Factors RLZ          : " + std::to_string(rlz_factors) + "\n";
    output = output + "Factors Total        : " + std::to_string(total_factors) + "\n";
    output = output + "--------------------------------------------" + "\n";
  }
  void ReportTime(std::string& output) {
    output = output + "Time SA          : " + std::to_string(time_sa) + "\n";
    output = output + "Time LZ77 of ref : " + std::to_string(time_parse_ref) + "\n";
    output = output + "Time RLZ parse   : " + std::to_string(time_rlz) + "\n";
    output = output + "-------------------------------" + "\n";
    output = output + "Time RLZ total   : " + std::to_string(time_total) + "\n";
  }

  size_t NaiveParsePrefix();
  size_t ComputeBlockLen();

  size_t process_next_block(size_t block_offset);

  void chunk_parse(text_t* buffer,
                   size_t length,
                   std::vector<Factor> & ans,
                   FMD::FastaMetaData * metadata,
                   size_t abs_starting_pos);

  void read_block(text_t *buffer, size_t buffer_len, size_t block_id, FILE * input_fp);

  size_t flush_phrases(std::vector<std::vector<Factor>> factor_lists);

  ParseStreamer * getParseStreamer();

  ~RlzParser();
  size_t parse_ref();
  size_t kkp_parse_ref(text_t * seq,
                       sa_t* sa,
                       size_t seq_len,
                       size_t offset);

  public:
  // input info
  bool is_already_parsed;
  std::string input_filename;
  std::string output_filename;
  size_t reference_len;
  size_t ignore_len;
  size_t n_threads;
  size_t n_partitions;
  size_t max_memory_MB;
  bool is_fasta;
  int verbosity;

  size_t text_len_bytes;
  size_t text_len;

  BlockReader<text_t> block_reader;

  void ComputeSumsStream(std::string out_filename);
  Dictionary<text_t, sa_t> d;
  ParseWriter writer;
  // output info
  size_t skipped_area_factors;
  size_t lz77_factors;
  size_t rlz_factors;
  size_t total_factors;

  long double time_sa;
  long double time_parse_ref;
  long double time_rlz;
  long double time_total;
};

#endif  // ReLZ_SRC_RLZ_PARSER_H_
