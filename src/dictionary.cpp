/* ReLZ
    Copyright (C) 2018 Daniel Valenzuela

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see http://www.gnu.org/licenses/ .
*/

#include <cinttypes>
#include <cstdint>
#include <cassert>
#include <cstdlib>

#include <divsufsort.h>
#include <divsufsort64.h>
#include <sdsl/int_vector.hpp>
#include <sdsl/qsufsort.hpp>

#include "./dictionary.h"
#include "./tools.h"

using std::cin;
using std::cout;
using std::endl;
using sdsl::int_vector;

template <typename text_t, typename sa_t>
Dictionary<text_t, sa_t>::Dictionary() {
}

template <typename text_t, typename sa_t>
void Dictionary<text_t, sa_t>::Init(text_t * text,
                                    size_t text_len,
                                    size_t ignored_len,
                                    int _n_threads) {
  d = text;
  n = text_len;
  n_threads = _n_threads;
  assert(n > 0);
  offset_ignored = ignored_len;
}

template <typename text_t, typename sa_t>
void Dictionary<text_t, sa_t>::Close() {
  delete[] sa;
  delete[] d;
}

template <typename text_t, typename sa_t>
void Dictionary<text_t, sa_t>::BuildSA() {
  assert(sizeof(sa_t) >= sizeof(text_t));
  sa = new sa_t[n];
  {
    int_vector<> tmp_sa(n + 1);
    int_vector<> tmp_text(n + 1);
    for (size_t i = 0; i < n; i++) {
      tmp_text[i] = d[i]+1;
    }
    tmp_text[n] = 0;
    sdsl::util::bit_compress(tmp_text);
    sdsl::qsufsort::construct_sa(tmp_sa, tmp_text);

    for (size_t i = 0; i < n; i++) {
      sa[i] = tmp_sa[i+1];
    }
    tmp_sa.resize(0);
    tmp_text.resize(0);
  }
  // #ifndef NDEBUG
  // TODO(FUTURE): sufcheck for integer sequences?
  // #endif
}

template <typename text_t, typename sa_t>
size_t Dictionary<text_t, sa_t>::size_in_bytes(size_t given_len) {
  return given_len*(sizeof(text_t)+sizeof(sa_t));
}

template <typename text_t, typename sa_t>
size_t Dictionary<text_t, sa_t>::size_in_bytes() {
  return size_in_bytes(n);
}

template <>
void Dictionary<uint8_t, uint32_t>::BuildSA() {
  sa = new uint32_t[n];
  divsufsort(d, (int32_t*)sa, (int32_t)n);
#ifndef NDEBUG
  if (sufcheck(d, (int32_t*)sa, (int32_t)n, false)) {
    cout << "Sufcheck failed!" << endl;
    exit(33);
  }
#endif
}

template <>
void Dictionary<uint8_t, uint64_t>::BuildSA() {
  sa = new uint64_t[n];
  divsufsort64(d, (int64_t*)sa, (int64_t)n);
#ifndef NDEBUG
  if (sufcheck64(d, (int64_t*)sa, (int64_t)n, false)) {
    cout << "Sufcheck failed!" << endl;
    exit(33);
  }
#endif
}

template struct Dictionary<uint8_t, uint32_t>;
template struct Dictionary<uint8_t, uint64_t>;
template struct Dictionary<uint64_t, uint64_t>;
