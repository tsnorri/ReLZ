/* ReLZ
   Copyright (C) 2018 Daniel Valenzuela

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see http://www.gnu.org/licenses/ .
*/
#ifndef ReLZ_SRC_PARSE_WRITER_H_
#define ReLZ_SRC_PARSE_WRITER_H_

#include <string>
#include "./tools.h"
#include "./io/async_stream_writer.hpp"

// TODO(CODEQUAL): remame to parse encoder?
class ParseWriter{
  public:
    ParseWriter();
    explicit ParseWriter(std::string filename);
    void Open(std::string filename);
    void Save(Factor * ptr, size_t count);
    void Close();
    ~ParseWriter();
    void EncodeAndWriteVbye(uint64_t value);

  private:
    io_private::async_stream_writer<uint8_t> writer;
};
#endif  // ReLZ_SRC_PARSE_WRITER_H_
