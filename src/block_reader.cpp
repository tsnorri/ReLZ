/* ReLZ
    Copyright (C) 2018 Daniel Valenzuela

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see http://www.gnu.org/licenses/ .
*/
#include <fstream>
#include <string>
#include <cassert>
#include <iostream>
#include "tools.h"
#include "block_reader.h"

using std::string;
using std::cerr;
using std::cout;
using std::endl;

template <typename text_t>
BlockReader<text_t>::BlockReader() {
}

template <typename text_t>
void BlockReader<text_t>::Init(string filename, size_t initial_offset_in_bytes, size_t block_len_, int verbosity_) {
  block_len = block_len_;
  text_len_bytes = Tools::file_size_or_fail(filename.c_str());
  assert((text_len_bytes % sizeof(text_t)) == 0);
  text_len = text_len_bytes/sizeof(text_t);
  fp = Tools::open_file_or_die(filename, "r");
  fseek(fp, (long int)initial_offset_in_bytes, SEEK_SET);
  verbosity = verbosity_;
  b_id = 0;
  processed_len = initial_offset_in_bytes/sizeof(text_t);
  buffer_main = new text_t[block_len + 1];
  buffer_next = new text_t[block_len + 1];
  cleaned_up = false;
}

// THIS IS CALLED ONLY ONCE.
template <typename text_t>
void BlockReader<text_t>::LoadFirstBlock(text_t * buffer_ext, size_t buffer_len) {
  assert(processed_len + buffer_len <= text_len);
  assert(!IsReady());
  
  total_blocks = (text_len - processed_len - buffer_len + block_len - (size_t)1)/ block_len;
  
  //b_id++;
  if (verbosity >= 2)
    cout << "Reading block " << b_id << "/" << total_blocks << endl;
  long double start_time= Tools::wclock();
  if (buffer_len != fread(buffer_ext, sizeof(text_t), buffer_len, fp)) {
    printf("ERR");
    exit(-1);
  }
  long double end_time= Tools::wclock();
  if (verbosity >= 2)
    cout << "Block read in " << end_time - start_time << " (s)" << endl;
  processed_len += buffer_len;
}

template <typename text_t>
void  BlockReader<text_t>::AsynchPreLoadNext() {
#pragma omp task
  PreLoadNext();
}

template <typename text_t>
void  BlockReader<text_t>::PreLoadNext() {
  if(IsReady()) 
    return;

  buffer_next_len = (processed_len + block_len < text_len) ? block_len : text_len - processed_len;
  b_id++;
  if (verbosity >= 2) {
    cout << "Reading block " << b_id << "/" << total_blocks << endl;
    fflush(stdout);
  }
  long double start_time= Tools::wclock();
  if (buffer_next_len != fread(buffer_next, sizeof(text_t), buffer_next_len, fp)) {
    printf("ERR");
    exit(-1);
  }
  long double end_time= Tools::wclock();
  if (verbosity >= 2) {
    cout << "Block read in " << end_time - start_time << " (s)" << endl;
    fflush(stdout);
  }
/////////////////////////
}

template <typename text_t>
text_t * BlockReader<text_t>::GetNextBlock(size_t * ans_buffer_len) {
  if (verbosity >= 2) {
    cout << "Waiting for all tasks to be done..." << endl;
    fflush(stdout);
  }
#pragma omp taskwait
  if (verbosity >= 2) {
    cout << "Done." << endl;
    fflush(stdout);
  }
  text_t * tmp = buffer_main; 
  buffer_main = buffer_next;
  buffer_next = tmp;
  *ans_buffer_len = buffer_next_len;
  processed_len += buffer_next_len;
  AsynchPreLoadNext();
  return buffer_main;
}

template <typename text_t>
void BlockReader<text_t>::Close() {
  fclose(fp);
	delete [] buffer_main;
	delete [] buffer_next;
  cleaned_up = true;
}

template <typename text_t>
BlockReader<text_t>::~BlockReader() {
  if (!cleaned_up) 
    Close();
}

template class BlockReader<uint8_t>;
template class BlockReader<uint64_t>;
