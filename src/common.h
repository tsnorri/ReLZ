/* ReLZ
   Copyright (C) 2018 Daniel Valenzuela

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see http://www.gnu.org/licenses/ .
*/
#include <assert.h>
#include <cstdint>
#ifndef ReLZ_SRC_COMMON_H_
#define ReLZ_SRC_COMMON_H_

enum class EncodingType { PLAIN, VBYTE, ELIAS, MAX, UNDEF };
enum class PointerType{ POS, DIST, UNDEF };

const std::string MULTI_SUFFIXES[4] = { ".pos.1.fse", ".pos.2.fse", ".len.fse", ".z"};
// TEST have been done with smaller value, to observe "multi" output also in smaller files.
const uint64_t  THRESH = UINT32_MAX;


struct Factor{
  using pos_t = uint64_t;
  using len_t = uint64_t;
  // It would be possible to change to len_t to uint32_t.
  // That will need to make sure that not only dictionary,
  // nut also re-parse keeps lengths bounded...
  pos_t pos;
  len_t len;
};

#endif  // ReLZ_SRC_COMMON_H_
