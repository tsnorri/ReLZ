/* ReLZ
    Copyright (C) 2018 Daniel Valenzuela

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see http://www.gnu.org/licenses/ .
*/
#ifndef ReLZ_SRC_TOOLS_H_
#define ReLZ_SRC_TOOLS_H_
#include <stdint.h>
#include <unistd.h>
#include <sys/time.h>
#include <errno.h>

#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <string>
#include <algorithm>
#include <vector>
#include "./common.h"


namespace Tools {
  FILE * open_file(const char *path, size_t *n);
  long double wclock();
  size_t MaxRefFromMemBytes(size_t mem_bytes, size_t bytes_per_text_t, bool force_large);
  void fasta_to_seq(uint8_t *text, size_t text_len, size_t *seq_len);
  bool is_breakline(uint8_t c);

  size_t file_size_or_fail(const char *path);
  FILE *open_file_or_die(const char * path, std::string mode);
  FILE *open_file_or_die(std::string fname, std::string mode);
  void remove_file_or_die(std::string fname);
  void rename_file_or_die(std::string first, std::string second);
  
  void SaveNum(uint64_t num, std::string filename);
  uint64_t LoadNum(std::string filename);

  template<typename data_t> void ReportMBESize(std::vector<data_t> data);
  template<typename data_t> void save_vector(FILE * fp, std::vector<data_t> vec);
  template<typename text_t> void read_typed(const char *filename, text_t* &text, uint64_t &length);
  template<typename curr_t> uint32_t bits(curr_t value);
  template<typename text_t> text_t *load_prefix(const char *path,
                                                size_t reference_length,
                                                size_t ignore_length);
  template<typename text_t> text_t *load_prefix_fasta(const char *path,
                                                      size_t reference_length,
                                                      size_t *seq_len);
}  // namespace Tools

#endif  // ReLZ_SRC_TOOLS_H_
