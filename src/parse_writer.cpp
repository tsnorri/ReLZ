/* ReLZ
    Copyright (C) 2018 Daniel Valenzuela

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see http://www.gnu.org/licenses/ .
*/
#include "parse_writer.h"
#include <fstream>
#include <string>
#include <cassert>
#include <cstdio>
#include <iostream>
#include "tools.h"

using std::string;
using std::cerr;
using std::endl;

ParseWriter::ParseWriter() {
}

ParseWriter::ParseWriter(string filename) {
	this->Open(filename);
}


void ParseWriter::Open(string filename) {
  writer.open(filename.c_str());
}

void ParseWriter::Close() {
  writer.close();
}

void ParseWriter::Save(Factor * ptr, size_t count) {
	using pos_t = typename Factor::pos_t;
	using len_t = typename Factor::len_t;
   
	for (size_t i = 0; i < count; i++) {
		pos_t pos = ptr[i].pos;
		len_t len = ptr[i].len;
    EncodeAndWriteVbye(pos); 
    EncodeAndWriteVbye(len); 
	}
}

void ParseWriter::EncodeAndWriteVbye(uint64_t value) {
  while (value > 127) {
    writer.write((value & 0x7f) | 0x80);
    value >>= 7;
  }
  writer.write(value);
}

ParseWriter::~ParseWriter() {	
}

