/* ReLZ
    Copyright (C) 2018 Daniel Valenzuela

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see http://www.gnu.org/licenses/ .
*/
#ifndef ReLZ_SRC_PARSE_STREAMER_H_
#define ReLZ_SRC_PARSE_STREAMER_H_

#include <string>
#include "./io/async_vbyte_stream_reader.hpp"
#include "./tools.h"

class ParseStreamer{
  public:
    ParseStreamer();
    explicit ParseStreamer(std::string filename);
    inline Factor getNextFactor() {
      Factor ans;
      ans.pos = reader.read();
      ans.len = reader.read();
      return ans;
    }
    bool isEmpty() {
      return this->reader.empty();
    }
    ~ParseStreamer();

  private:
    std::string filename;
    io_private::async_vbyte_stream_reader<uint64_t> reader;
};
#endif  // ReLZ_SRC_PARSE_STREAMER_H_
