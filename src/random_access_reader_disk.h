/* ReLZ
    Copyright (C) 2018 Daniel Valenzuela

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see http://www.gnu.org/licenses/ .
*/
#ifndef ReLZ_SRC_RANDOM_ACCESS_READER_DISK_H_
#define ReLZ_SRC_RANDOM_ACCESS_READER_DISK_H_

#include <string>
#include <fstream>
#include <iostream>
#include "./random_access_reader.h"
#include "./tools.h"

template <typename T>
class RandomAccessReaderDisk : public RandomAccessReader<T> {
  public:
    RandomAccessReaderDisk();
    explicit RandomAccessReaderDisk(std::string filename);
    T get(size_t pos) override {
      assert(in_fs.tellg() == _i*(int64_t)sizeof(T));
      if ((int64_t)pos != _i) {
        int64_t delta = (int64_t)pos - _i;
        in_fs.seekg((int64_t)(delta*(int64_t)sizeof(T)), std::ios_base::cur);
        _i += delta;
        assert(_i > 0);
        assert(in_fs.tellg() == _i*(int64_t)sizeof(T));
      }
      T ans;
      in_fs.read((char*)&ans, sizeof(ans));
      _i++;
      assert(in_fs.tellg() == _i*(int64_t)sizeof(T));
      return ans;
    }
    ~RandomAccessReaderDisk();

  private:
    std::ifstream in_fs;
    int64_t __size;
    int64_t _i;
};
#endif  // ReLZ_SRC_RANDOM_ACCESS_READER_DISK_H_
