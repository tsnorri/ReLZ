#include <cstdio>
#include <iostream>
#include <fstream>
#include <cstdlib>
#include <stdint.h>

#include "stream.h"

#include <sdsl/vectors.hpp>
#include <sdsl/coder_elias_delta.hpp>

template<typename text_t> 
void Decode(const char * input,
						const char * output,
						long prefix_size) {
  
  std::string tmp_input(input); 
  //sdsl::vlc_vector<sdsl::coder::elias_delta,UINT32_MAX/4,0> elias_vec;
  sdsl::vlc_vector<sdsl::coder::elias_delta,8192,0> elias_vec;
  sdsl::load_from_file(elias_vec, tmp_input.c_str());

  fprintf(stderr, "Input file: %s\n", input);
  fprintf(stderr, "Output file: %s\n", output);

  text_t *prefix= new text_t[prefix_size];
  std::ofstream ofs(output);
  
  if (!ofs.good()) {
    fprintf(stderr, "Error opening %s \n.", output);
    exit(EXIT_FAILURE);
  }
  

  uint64_t ptr = 0, dbg = 0, tot = 0;
	text_t tmp;

  //while (!(reader->empty())) {
  size_t i = 0;
  assert(elias_vec.size() % 2 == 0);
  uint64_t curr_pos = 0;
  while (i < elias_vec.size()) {
    // This way of decoding is extremely slo as it always decode from the previous sample (8192).
    uint64_t dist = elias_vec[i];
    uint64_t len = elias_vec[i+1];
    i += 2;
    

    if (!len) {
      if (ptr < prefix_size) {
        prefix[ptr++] = (text_t)dist;
      }
      //ofs.put((text_t)dist);
     	tmp = (text_t)dist;
			ofs.write((char*)(&tmp), sizeof(tmp));
    }
    else {
      uint64_t pos = curr_pos - dist;
      for (uint64_t j = 0; j < len; ++j) {
        if (ptr < prefix_size) {
          prefix[ptr++] = prefix[pos + j];
        }
        //ofs.put(prefix[pos+j]);
     		tmp = (text_t)prefix[pos+j];
				ofs.write((char*)(&tmp), sizeof(tmp));
      }
    }

    // Print progress.
    uint64_t phrase_len = len ? len : 1;
    curr_pos += phrase_len;
    if (curr_pos - dbg > (10 << 20)) {
      dbg = curr_pos;
      fprintf(stderr, "Decompressed %.2LfMiB \r",
          (long double)curr_pos / (1 << 20));
    }
  }

  if (!ofs.good()) {
    fprintf(stderr, "An error occurred with the output file.\n");
  }
  fprintf(stderr, "DONE\n");
  delete[] prefix;

}
