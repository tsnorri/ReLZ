#!/usr/bin/env bash
set -o errexit
set -o nounset

DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )
SDSL_DIR="sdsl-lite-2.1.1"

function check_for_sdsl_in_parent 
{
	ATTEMPT_ROUTE="${DIR}/../../${SDSL_DIR}/Make.helper"
	if [[ -f "${ATTEMPT_ROUTE}" ]]; then
		echo "SDSL project found in parent directory. Using that one..."
		echo "include ${ATTEMPT_ROUTE}" >  Make.route_to_sdsl
		exit 0
	else
		echo "No SDSL foud in parent..."
	fi
}

function install_sdsl_locally 
{
	echo "Installing sdsl from bash script..."
	wget https://github.com/simongog/sdsl-lite/releases/download/v2.1.1/sdsl-lite-2.1.1.zip.offline.install.zip
	unzip sdsl-lite-2.1.1.zip.offline.install.zip
	local SDSL_TARGET_DIR="sdsl-local-install"
	cd "./${SDSL_DIR}"
	./install.sh ../${SDSL_TARGET_DIR}
	cd ..
	echo "include ${DIR}/${SDSL_DIR}/Make.helper" >  Make.route_to_sdsl
	rm -rf sdsl-lite-2.1.1.zip.offline.install.zip
}

function real_helper
{
	## Reads the first non-comment line of the input, and outputs the first tring after a whitespace
	FILENAME=${1}
	while read LINE
	do
		#echo ${LINE}
		if [ ! "${LINE:0:1}" == "#" ] # Error on this line
		then
			echo ${LINE} | cut -d " " -f 2
			return
		fi
	done < ${FILENAME} 
}


HELPER_FILE=${DIR}/Make.route_to_sdsl
if [[ ! -f "${HELPER_FILE}" ]]; then
	check_for_sdsl_in_parent	
	install_sdsl_locally
else
	ROUTE_TO_SDSL_HELPER=$(real_helper ${HELPER_FILE})
	#echo "Real Helper is: ${ROUTE_TO_SDSL_HELPER}"
	if [[ ! -f "${ROUTE_TO_SDSL_HELPER}" ]]; then
		check_for_sdsl_in_parent	
		install_sdsl_locally
	else
		echo "LIBSDSL IS ALREADY INSTALLED."
		rm -rf sdsl-lite-2.1.1.zip.offline.install.zip
	fi
fi
